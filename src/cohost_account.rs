use serde::Deserialize;

/// The API URL from whence Cohost serves JSON project definitions
pub const COHOST_ACCOUNT_API_URL: &str = "https://cohost.org/api/v1/project/";

#[derive(Debug, Deserialize, PartialEq, Eq)]
pub struct CohostAccount {
    #[serde(rename = "projectId")]
    project_id: u64,
    #[serde(rename = "displayName")]
    display_name: String,
    dek: String,
    description: String,
}

#[test]
fn deserialize_account() -> Result<(), Box<dyn std::error::Error>> {
    let example = include_str!("../samples/cohost/api/v1/project.json");
    let expected_account = CohostAccount {
        project_id: 8891,
        display_name: "nora (noracodes@weirder.earth)".into(),
        dek: "queer pagan computer toucher".into(),
        description: "go follow me on [the fediverse](https://weirder.earth/@noracodes)\r\n\r\nhi! i'm nora. i was born under the [Great Comet of the Millennium](https://en.wikipedia.org/wiki/Comet_Hale%E2%80%93Bopp). i'm a 🦀👩‍💻\r\nrustacean, 🍄🔮 witch, 📡📻 radio amateur, hacker, synthesist, and general 🚩🏴 leftie nerd who lives on the outskirts of chicago in a little condo with my polycule.\r\n\r\n\"and when the last of those fires let fall / there was no lord in the world at all.\"".into(),
    };
    let actual_account: CohostAccount = serde_json::from_str(example)?;
    assert_eq!(expected_account, actual_account);
    Ok(())
}