use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct CohostWebfingerResource {
    subject: String,
    aliases: Vec<String>,
    links: Vec<CohostWebfingerProfileLink>,
}

#[derive(Debug, Serialize)]
pub struct CohostWebfingerProfileLink {
    rel: String,
    #[serde(rename = "type")]
    t_type: String,
    href: String,
}

impl CohostWebfingerResource {
    pub fn new<S: AsRef<str>, T: AsRef<str>>(project_id: S, domain: T) -> Self {
        Self {
            subject: format!("acct:{}@{}", project_id.as_ref(), domain.as_ref()),
            aliases: vec![format!("acct:{}@cohost.org", project_id.as_ref())],
            links: vec![CohostWebfingerProfileLink::new(project_id)],
        }
    }
}

impl CohostWebfingerProfileLink {
    pub fn new<S: AsRef<str>>(project_id: S) -> Self {
        Self {
            rel: "http://webfinger.net/rel/profile-page".into(),
            t_type: "text/html".into(),
            href: format!("https://cohost.org/{}", project_id.as_ref())
        }
    }
}

#[test]
fn serialize_webfinger_resource() -> Result<(), Box<dyn std::error::Error>> {
    let expected_json = include_str!("../samples/northbound-train/.well-known/webfinger");
    let resource: CohostWebfingerResource = CohostWebfingerResource::new("noracodes", "example.com");
    let actual_json = serde_json::to_string_pretty(&resource)?;
    println!("{}", actual_json);
    assert_eq!(&expected_json, &actual_json);
    Ok(())
}