# Northbound Train

Translate Cohost users and posts into ActivityPub actors and activities.

> Till we feel the far track humming,
> and we see her headlight plain,
> and we gather and wait her coming --
> the wonderful north-bound train.
>
> - *Bridge-Guard in the Karroo*, Rudyard Kipling

## What it currently does

Right now northbound-train just proxies Webfinger requests to Cohost project lookups using the v1 API.

Place it at `.well-known/webfinger` and set your domain name with `--domain`.

If you must, you can configure the base URL with `--base-url`.

## Todo

- [x] Webfinger for Cohost users
    - [ ] Handle redirects if they exist (?)
- [ ] Expose ActivityPub actors for Cohost users
- [ ] Permit following ActivityPub actors for Cohost users
- [ ] Deliver posts from Cohost users to ActivityPub followers